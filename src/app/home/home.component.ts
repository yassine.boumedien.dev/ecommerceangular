import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../services/article.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products=[];
  constructor(private _articleservice:ArticleService) { }
  top3=[]
  ngOnInit(): void {
    let iduser=parseInt(JSON.parse(localStorage.getItem("currentUser")));
    this._articleservice.getsimsarticle(iduser).subscribe((res:any)=>{
      console.log(res);
      this.products=res.prod
      this._articleservice.gettop3().subscribe((res:any)=>{
        this.top3=res.list;
        console.log(this.top3)
      })

    })
  }

}
