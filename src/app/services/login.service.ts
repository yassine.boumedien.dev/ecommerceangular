import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor( private http: HttpClient) {
      this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
      return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    

    const header = {
       headers : new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
    })
    };
    let body = `user=${username}&passs=${password}`;
    let apiUrl='http://127.0.0.1:5000'
      return this.http.post<any>(`${apiUrl}/login`,
      body, header);

        
    }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');

    this.currentUserSubject.next(null);
}
}
