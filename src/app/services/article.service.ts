import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  constructor(private http: HttpClient){

  }
  /*header = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Origin': '*'

    }),
 
  };*/
  apiUrl='http://127.0.0.1:5000'
  getallarticles():Observable<any>{
    return this.http.get(this.apiUrl + '/articles');
  
 }
 getrecommended(id):Observable<any>{
  return this.http.get(this.apiUrl + '/recommend/'+id);

}
 getbyid(id):Observable<any>{
  return this.http.get(this.apiUrl + '/article/'+id);

}
noter(iduser: Number, idprod: Number,note:Number) {
    

  const header = {
     headers : new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
  })
  };
  let body = `user=${iduser}&pdt=${idprod}&note=${note}`;
  let apiUrl='http://127.0.0.1:5000'
    return this.http.post<any>(`${apiUrl}/note`,
    body, header);

      
  }



  getnote(idproduit,iduser):Observable<any>{
    return this.http.get(this.apiUrl + '/getnote/'+idproduit+'/'+iduser);
  
  }

  getsimsarticle(iduser):Observable<any>{
    return this.http.get(this.apiUrl + '/recommenduser/'+iduser);
  
  }


  gettop3():Observable<any>{
    return this.http.get(this.apiUrl + '/top3');
  
  }

}
