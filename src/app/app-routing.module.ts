import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { StoreComponent } from './store/store.component';
import { ProduitComponent } from './produit/produit.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {
  path:"",component:HomeComponent
},
{
  path:"store",component:StoreComponent
},
{
  path:"produit/:id",component:ProduitComponent
},
{
  path:"login",component:LoginComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
