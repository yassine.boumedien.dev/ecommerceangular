import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _loginservice:LoginService,private _Router:Router) { }

  ngOnInit(): void {
  }
username;
password;
  login(){
    this._loginservice.login(this.username,this.password).subscribe((res:any)=>{
      console.log(res);
      if (res ) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(res.user));
        localStorage.setItem('currentName', JSON.stringify(res.name));

        this._Router.navigate(['/'])
    }
    })


  }

}
