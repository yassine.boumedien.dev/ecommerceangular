import { Component, OnInit } from '@angular/core';
import { LoginService } from './services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  name="My Account"
  constructor(private _authservice:LoginService,private _router:Router){}
  ngOnInit(): void {
    let name=JSON.parse(localStorage.getItem('currentName'));
    if (name)
    this.name=name;
    }

  logout(){
    this._authservice.logout();
    this._router.navigate(['/login']);
    this.name= "My Account"


  }
  title = 'ecommerceprojet';
  
}
