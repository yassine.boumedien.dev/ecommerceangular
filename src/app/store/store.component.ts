import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../services/article.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {

  constructor(private _articleservice:ArticleService) { 

  }
products=[]
  ngOnInit(): void {
    this._articleservice.getallarticles().subscribe((res:any)=>{
      console.log(res)
      this.products=res.list
    })
  }

}
