import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../services/article.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.css']
})
export class ProduitComponent implements OnInit {

  constructor(private _articleservice: ArticleService, private _ActivatedRoute: ActivatedRoute) { }
  article;
  id
  hidebutton=false;
  note;
  products = []
  ngOnInit(): void {
    let iduser=parseInt(JSON.parse(localStorage.getItem("currentUser")));

    this.id = this._ActivatedRoute.snapshot.paramMap.get('id');
    this._articleservice.getbyid(this.id).subscribe((res: any) => {
      this.article = res.article;
      console.log(res.article)
      this._articleservice.getrecommended(this.id).subscribe((res: any) => {
        this.products = res.article;
      this._articleservice.getnote(this.id,iduser).subscribe((res:any)=>{
        if (res.note){
          console.log(res);
          this.hidebutton=true;
          this.note=parseInt(res.note);
        }
      })
      })
    })
  }

  
  noter() {
    let iduser=parseInt(JSON.parse(localStorage.getItem("currentUser")));

    this._articleservice.noter(iduser,this.id,this.note*2).subscribe((res:any)=>{
      console.log(res);
      this.hidebutton=true;

    })


  }

}
